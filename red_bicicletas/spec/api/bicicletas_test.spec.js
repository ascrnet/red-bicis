//jasmine spec/api/bicicletas_test.spec.js
var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:5000/api/bicicletas';

describe ('Bicicleta API',() => {

    beforeEach(function(done){
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.disconnect();
        mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true  });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error en la conexión'));
        db.once('open', function(){
            console.log('Conexion con testdb');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function( err, success ){
            if (err) console.log(err);
            done();
        });
    });

    describe ('Get Bicicletas',() => {
        it('Status 200 ', () => {
            request.get(url, function (error, response,body) {
                var result= JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicleta.length).toBe(0);
            });
        });       
    });

    describe ('POST Bicicletas /create',() => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id" : 10, "color" : "Roja", "modelo" : "Urbana", "lat": -33, "lng" : -70 }';
            request.post({
                headers : headers,
                url : base_url +'/create',
                body : aBici
            }, function (error,response,body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("Roja");
                expect(bici.ubicacion[0]).toBe(-33);
                expect(bici.ubicacion[1]).toBe(-70);
                done();
             });
         });       
    });

    /*
    describe ('POST Bicicletas /delete',() => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id" : 5, "color" : "Roja", "modelo" : "Urbana", "lat": -33, "lng" : -70 }';
            request.post({
                headers : headers,
                url : 'http://localhost:5000/api/bicicletas/create',
                body : aBici
            }, function (error,response,body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(5).color).toBe("Roja");
                done();
            });
        }); 
        
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var deleteid = '{ "id" : 5 }';
            request.delete({
                headers : headers,
                url : 'http://localhost:5000/api/bicicletas/delete',
                body : deleteid
            }, function (error,response,body){
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });
    */


});





